<%-- 
    Document   : login
    Created on : Apr 28, 2021, 2:17:25 PM
    Author     : jorge
--%>


<%

    String email = "";
    String contrasena = "";
    boolean recordar = false;
    
    if(request.getParameter("email") != null){
        email = request.getParameter("email");
    }

    if(request.getParameter("contrasena") != null){
        contrasena = request.getParameter("contrasena");
    }

    recordar = Boolean.valueOf(request.getParameter("remember"));


    if(!email.equals("") && !contrasena.equals("")){
    
        try {
            UsuarioWS.UsuarioWS_Service service = new UsuarioWS.UsuarioWS_Service();
            UsuarioWS.UsuarioWS port = service.getUsuarioWSPort();
             // TODO initialize WS operation arguments here
            
            // TODO process result here
            UsuarioWS.Usuario user = port.login(email, contrasena);
            
            if(user.getId() > 0){//Guardamos la sesion del usuario
                
                //Variable de session
                session.setAttribute("nombre", user.getNombre());
                session.setAttribute("email", user.getEmail());
                session.setAttribute("perfil", user.getPerfil());
                session.setAttribute("nickname", user.getNickname());
                
                
                
                if(recordar){// guardamos la session en una cookie
                    //Declaracion de cookies
                    Cookie cusuario = new Cookie("cnombre",user.getNombre());
                    Cookie cemail = new Cookie("cemail",user.getEmail());
                    Cookie cperfil = new Cookie("cperfil",user.getPerfil());
                    Cookie cnickname = new Cookie("cnickname",user.getNickname());
                    
                    int dias = 60*60*24*30; //1 min = 60 seg; 1hr = 60min; 1dia = 24 hrs; 30 dias 
                    //La vida de la cookies
                    cusuario.setMaxAge(dias);
                    cemail.setMaxAge(dias);
                    cperfil.setMaxAge(dias);
                    cnickname.setMaxAge(dias);
                    
                    //Cookies las aņadimos al response
                    response.addCookie(cusuario);
                    response.addCookie(cemail);
                    response.addCookie(cperfil);
                    response.addCookie(cnickname);
                }
                
                response.sendRedirect("index.jsp");
                
                
            }else{//Error usuario o contraseņa no validos
                response.sendRedirect("index.jsp?error=novalidos");
            }
            
        } catch (Exception ex) {//Error usuario o contraseņa no validos            
                response.sendRedirect("index.jsp?error=novalidos");
        }
    }else{//Error usuario o contraseņa vacios
        response.sendRedirect("index.jsp?error=vacios");
    }


%>
