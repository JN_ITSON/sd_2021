<%



    if(session != null){
    
        session.setAttribute("nombre", "");
        session.setAttribute("email", "");
        session.setAttribute("perfil", "");
        session.setAttribute("nickname", "");
        
        session.invalidate();
        
    }
    
    
    if(request.getCookies() != null){
        for(Cookie c : request.getCookies()){
            c.setMaxAge(0);
            response.addCookie(c);
        }
    }


    response.sendRedirect("index.jsp");

%>