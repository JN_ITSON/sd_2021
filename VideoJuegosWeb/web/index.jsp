<%-- 
    Document   : index
    Created on : 08-mar-2021, 14:25:10
    Author     : jorge
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%
    List<CategoriaWS.Categoria> categorias = new ArrayList();
  

    try {
	CategoriaWS.CategoriaWS_Service service = new CategoriaWS.CategoriaWS_Service();
	CategoriaWS.CategoriaWS port = service.getCategoriaWSPort();
	// TODO process result here
	categorias = port.buscarTodosCategoria();
	
    } catch (Exception ex) {
	// TODO handle custom exceptions here
    }
    

    String nombre = "";
    String email = "";
    String nickname = "";
    String perfil = "";
    
    if(request.getCookies() != null){
        
        for(Cookie c : request.getCookies()){
            String nombreC = c.getName();
            
            if(nombreC.equals("cnombre")){
                session.setAttribute("nombre", c.getValue());
            }
            
            if(nombreC.equals("cemail")){
                session.setAttribute("email", c.getValue());
            }
            
            if(nombreC.equals("cnickname")){
                session.setAttribute("nickname", c.getValue());
            }
            
            if(nombreC.equals("cperfil")){
                session.setAttribute("perfil", c.getValue());
            }
            
        }
        
    }
    
    if(session != null){
        nombre = session.getAttribute("nombre") != null ? session.getAttribute("nombre").toString() : "";
        email = session.getAttribute("email") != null ? session.getAttribute("email").toString() : "";
        nickname = session.getAttribute("nickname") != null ? session.getAttribute("nickname").toString() : "";
        perfil = session.getAttribute("perfil") != null ? session.getAttribute("perfil").toString() : "";
    }



%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
        <link href="css/principal.css" rel="stylesheet" type="text/css"/>
        <link href="css/targeta.css" rel="stylesheet" type="text/css"/>
        <link href="css/grid-gallery.css" rel="stylesheet" type="text/css"/>
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    </head>
    <body>
        
        <div id="contenedor-principal" class="container">
            
            <div id="contenedor-header">
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
                    <div class="container-fluid">
                      <a class="navbar-brand" href="#">LOGOTIPO</a>
                      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                      </button>
                      <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item dropdown">
                                <a class="nav-link " href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                  <i class="fas fa-th-large"> Categorias</i>
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    
 
                                  <%
                                      
                                    for(CategoriaWS.Categoria cat : categorias ){

                                  %>
                                    <li><a class="dropdown-item" href="juegoCategoria.jsp?idCat=<%= cat.getId() %>"> <%= cat.getNombre() %> </a></li>
                                  
                                  <%
                                      }
                                  %>
                                  
                                </ul>
                            </li>
                        </ul>
                        <form class="d-flex">
                            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                                <%
                                
                                    if(nombre.isEmpty() || nombre.equals("")){

                                %>
                                        <li class="nav-item">
                                            <a class="nav-link" aria-current="page" href="#" data-bs-toggle="modal" data-bs-target="#loginModal">LOGIN</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" aria-current="page" href="#" data-bs-toggle="modal" data-bs-target="#registroModal">REGISTER</a>
                                        </li>
                                <%
                                    }else {
                                
                                %>
                                    <li class="nav-item">
                                        <a class="nav-link" aria-current="page"  > <%= nombre %></a>
                                    </li>
                                    
                                    <li class="nav-item">
                                        <a class="nav-link" aria-current="page" href="logout.jsp"  >Logout</a>
                                    </li>
                                
                                <%
                                    }
                                %>
                            </ul>
                        </form>
                      </div>
                    </div>
                  </nav>
                
                
                <div class="row">
                    <div class="col">
                        <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                            <div class="carousel-indicators">
                              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                            </div>
                            <div class="carousel-inner">
                              <div class="carousel-item active">
                                <img src="images/bg-slider-1.jpg" class="d-block w-100" alt="Wallpaper Valorant"/>
                              </div>
                              <div class="carousel-item">
                                <img src="images/bg-slider-2.jpg" class="d-block w-100" alt="Wallpaper Ragnarok Online"/>
                              </div>
                              <div class="carousel-item">
                                <img src="images/bg-slider-3.jpg" class="d-block w-100" alt="Wallpaper Fortnite"/>
                              </div>
                            </div>
                          </div>
                    </div>
                </div>
            </div> <!-- FIN DE HEADER -->
            
            <div id="contenedor-contenido" class="container">
                
                <div class="row">
                    <div class="col-md-1">&nbsp;</div>
                    <div class="col-md-10">
                        <h2>RECIENTES:</h2>
                    </div>
                    <div class="col-md-1">&nbsp;</div>
                </div>
                
                <div class="row">
                    
                    <div class="col-md-1">&nbsp;</div>
                    
                    
                        
                        
                        <%
                            for(int i=0; i<5; i++){
                        %>
                        <div class="col-md-2">                          
                            <div class="targeta-reciente">
                                <img src="https://static.posters.cz/image/1300/poster/minecraft-charged-creeper-i76673.jpg" alt="">
                                
                                <div class="targeta-reciente-contenido">
                                    <div class="row fila-uno-targeta">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-12 texto-targeta-principal">Minecraft</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 texto-targeta-principal">1.16.5</div>
                                            </div>
                                        
                                        </div>
                                        
                                        <div class="col-md-6 ">
                                            <div class="valoracion-targeta">7.5</div>
                                        </div>                                        
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-12 texto-targeta-secundario">U.S.A. 1990</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 texto-targeta-secundario">$49.99</div>
                                            </div>
                                        
                                        </div>
                                        
                                        <div class="col-md-6 icono-descarga">
                                            <i class="fas fa-cloud-download-alt"></i>
                                        </div>                                        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                        <%
                        }
                        
                        %>
                        
                   
                    
                    <div class="col-md-1">&nbsp;</div>
                </div>
                
                <div class="row mt-5">
                    <div class="col-md-1">&nbsp;</div>
                    <div class="col-md-10">
                        <h2>JUEGOS POR CATEGORIAS</h2>
                    </div>
                    <div class="col-md-1">&nbsp;</div>
                </div>
                        
                <div class="row grid-gallery">
                    <div class="col-md-1">&nbsp;</div>
                    
                    <div class="col-md-6">
                        <div>
                            <img src="images/bg-slider-1.jpg" alt=""/>
                            <span>
                                Shooter
                            </span>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div>
                            <img src="images/bg-slider-2.jpg" alt=""/>
                            <span>
                                RPG
                            </span>
                        </div>
                    </div>
                    
                    <div class="col-md-1">&nbsp;</div>
                </div>
                        
                 <div class="row grid-gallery">
                    <div class="col-md-1">&nbsp;</div>
                    
                    <div class="col-md-4">
                        <div>
                            <img src="images/bg-slider-3.jpg" alt=""/>
                            <span>
                                Platform
                            </span>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div>
                            <img src="images/bg-slider-2.jpg" alt=""/>
                            <span>
                                Adventure
                            </span>
                        </div>
                    </div>
                    
                    <div class="col-md-1">&nbsp;</div>
                </div>        
                
                        
                <div class="rainbow"></div>        
                        
            </div><!-- FIN DE CONTENIDO -->
            
            <div id="contenedor-footer">
                
            </div><!-- FIN DE FOOTER -->
          
        </div><!-- FIN DE CONTENEDOR PRINCIPAL -->
        
        
        
        <!-- Modal Login-->
        <form action="login.jsp" method="POST">
            <div class="modal fade" id="loginModal" tabindex="-1" aria-labelledby="loginModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Login ...</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                    <div class="container">   
                        <div class="row mb-3">
                            <div class="col">
                                <div class="input-group flex-nowrap">
                                    <span class="input-group-text" id="addon-wrapping">@</span>
                                    <input type="text" name="email" class="form-control" placeholder="Email ..." aria-label="Username" aria-describedby="addon-wrapping">
                                </div>
                            </div>
                        </div>


                        <div class="row mb-3">
                            <div class="col">

                                <div class="input-group flex-nowrap">
                                    <span class="input-group-text" id="addon-wrapping">*</span>
                                    <input type="password" name="contrasena" class="form-control" placeholder="Password ..." aria-label="Username" aria-describedby="addon-wrapping">
                                </div>
                            </div>
                        </div>


                        <div class="row mb-3">
                            <div class="col">
                                 <div class="form-check">
                                    <input name="remember" class="form-check-input" type="checkbox" value="true" id="flexCheckDefault">
                                    <label class="form-check-label" for="flexCheckDefault">
                                      Recordarme 30 días..
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                      
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-success">Login</button>
                  </div>
                </div>
              </div>
            </div>
        </form>
        
        
        <!-- Modal Registro-->
        <div class="modal fade" id="registroModal" tabindex="-1" aria-labelledby="registroModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registrarse ...</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                Registro
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>
            </div>
          </div>
        </div>
        
        
        
    </body>
</html>
