/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservices;

import entidades.Alumno;
import interfaces.DAOAlumno;
import interfaces.DAOAlumnoImpl;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author jorge
 */
@WebService(serviceName = "AlumnoWS")
public class AlumnoWS {

    private DAOAlumno dao = new DAOAlumnoImpl();
    
    @WebMethod(operationName = "agregarAlumno")
    public boolean agregarAlumno(
            @WebParam(name = "nombre") String nombre,
            @WebParam(name = "edad") int edad,
            @WebParam(name = "carrera") String carrera,
            @WebParam(name = "materias") int materias
    ) {
        
        Alumno alumno = new Alumno();
        alumno.setNombre(nombre);
        alumno.setEdad(edad);
        alumno.setCarrera(carrera);
        alumno.setMaterias(materias);
        
        return dao.agregar(alumno);
    }
    
    
    @WebMethod(operationName = "obtenerTodosAlumno")
    public List<Alumno> obtenerTodosAlumno() {
        return dao.obtenerTodos();
    }
    
    
}
/*
    Diseñar un web service para el registro, edicion, eliminacion
    y consultas sobre un listado de videojuegos


    id,
    nombre,
    clasificacion, 
    categoria,
    url_image,
    url_descarga,
    valoracion,
    review;



*/