/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservices;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author jorge
 */
@WebService(serviceName = "Ejmplo1")
public class Ejmplo1 {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hola " + txt + " !";
    }
    
    @WebMethod(operationName = "suma")
    public double suma(
            @WebParam(name = "op1")  double op1,
            @WebParam(name = "op2") double op2
    ){
       
        return op1+op2;
    }
    
    
    /*  
        Web services 
        Parametros (nombre,edad,sexo,email) Usuario
        Metodo agregar (List)
        Metodo obtener List
        --> TAREA para la siguiente clase.
    */
    
    
}
