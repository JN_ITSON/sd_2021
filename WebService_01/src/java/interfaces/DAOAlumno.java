package interfaces;

import entidades.Alumno;
import java.util.List;


public interface DAOAlumno {
    public boolean agregar(Alumno alumno);
    public boolean editar(int id, Alumno alumno);
    public boolean eliminar(int id);
    public List<Alumno> obtenerTodos();
    public List<Alumno> obtenerPorNombre(String nombre);
    public Alumno obtenerPorId(int id);
}
