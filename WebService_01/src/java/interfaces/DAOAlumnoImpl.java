package interfaces;

import configuracion.DBHelper;
import entidades.Alumno;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class DAOAlumnoImpl implements DAOAlumno{

    private DBHelper db = new DBHelper();
    
    @Override
    public boolean agregar(Alumno alumno) {
        boolean resultado = false;
        
        try {
            if(db.connect()){
                String sql  = "INSERT INTO alumnos (nombre,edad,carrera,materias) "
                        + " VALUES ("
                            + "'"+alumno.getNombre()+"',"
                            + " "+alumno.getEdad()+" ,"
                            + "'"+alumno.getCarrera()+"',"
                            + " "+alumno.getMaterias()+""
                        + ")";
                
                resultado = (boolean) db.execute(sql,true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
           db.disconnect();
        }
        
        return resultado;
    }

    @Override
    public boolean editar(int id, Alumno alumno) {
        
        boolean resultado = false;
        
        try {
            if(db.connect()){
                String sql  = "UPDATE alumnos SET "
                            + " nombre = '"+alumno.getNombre()+"',"
                            + " edad = "+alumno.getEdad()+" ,"
                            + " carrera = '"+alumno.getCarrera()+"',"
                            + " materias = "+alumno.getMaterias()+""
                        + " WHERE id = "+id;
                
                resultado = (boolean) db.execute(sql,true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
           db.disconnect();
        }
        
        return resultado;
    }

    @Override
    public boolean eliminar(int id) {
        
        boolean resultado = false;
        
        try {
            if(db.connect()){
                String sql  = "DELETE FROM alumnos  "
                        + " WHERE id = "+id;
                
                resultado = (boolean) db.execute(sql,true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
           db.disconnect();
        }
        
        return resultado;
    }

    @Override
    public List<Alumno> obtenerTodos() {
        List<Alumno> alumnos = new ArrayList();
        
           
        try {
            if(db.connect()){
                String sql  = "SELECT * FROM alumnos  ";
                
                ResultSet rs = (ResultSet) db.execute(sql,false);
                
                while(rs.next()){
                    Alumno alumno = new Alumno();
                    
                    alumno.setId(rs.getInt("id"));
                    alumno.setNombre(rs.getString("nombre"));
                    alumno.setEdad(rs.getInt("edad"));
                    alumno.setCarrera(rs.getString("carrera"));
                    alumno.setMaterias(rs.getInt("materias"));
                    
                    alumnos.add(alumno);
                    
                }
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
           db.disconnect();
        }
        
        return alumnos;
    }

    @Override
    public List<Alumno> obtenerPorNombre(String nombre) {
        List<Alumno> alumnos = new ArrayList();
        
        try {
            if(db.connect()){
                String sql  = "SELECT * FROM alumnos WHERE"
                        + " nombre LIKE '%"+nombre+"%' ";
                
                ResultSet rs = (ResultSet) db.execute(sql,false);
                
                while(rs.next()){
                    Alumno alumno = new Alumno();
                    
                    alumno.setId(rs.getInt("id"));
                    alumno.setNombre(rs.getString("nombre"));
                    alumno.setEdad(rs.getInt("edad"));
                    alumno.setCarrera(rs.getString("carrera"));
                    alumno.setMaterias(rs.getInt("materias"));
                    
                    alumnos.add(alumno);
                    
                }
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
           db.disconnect();
        }
        
        return alumnos;
    }

    @Override
    public Alumno obtenerPorId(int id) {
            Alumno alumno = new Alumno();
        try {
            if(db.connect()){
                String sql  = "SELECT * FROM alumnos WHERE id = "+id;
                
                ResultSet rs = (ResultSet) db.execute(sql,false);
                
                if(rs.next()){
                    alumno.setId(rs.getInt("id"));
                    alumno.setNombre(rs.getString("nombre"));
                    alumno.setEdad(rs.getInt("edad"));
                    alumno.setCarrera(rs.getString("carrera"));
                    alumno.setMaterias(rs.getInt("materias"));                    
                }
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
           db.disconnect();
        }
        
        return alumno;
    }
    
}
