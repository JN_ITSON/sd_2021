<%-- 
    Document   : index
    Created on : 08-feb-2021, 14:29:08
    Author     : jorge
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        
        
        <%
            
            String nombres[] = {"Vinko","Juan Albañez","Figueroa","Serna"};
            
            String nombre = "Jorge Alberto";
            
            out.print(nombre);
        
        %>
        <table border="1">
            <tr>
                <td>Nombre:</td>
            </tr>
            <%
            
                for(String name : nombres){
                 out.print("<tr><td>"+name+"</td></tr>")   ;
                }
            
            %>
                        
        </table>
            <h2>Hola <%= nombre+" Mora " %></h2>
    </body>
</html>
