package componentes;

import javax.swing.Icon;
import javax.swing.JLabel;


public class LabelCustom extends JLabel{

    private int idGame = 0;
    private GamesWS.Games juego = null;
    
    public LabelCustom(Icon image) {
        super(image);
    }

    public int getIdGame() {
        return idGame;
    }

    public void setIdGame(int idGame) {
        this.idGame = idGame;
    }

    public GamesWS.Games getJuego() {
        return juego;
    }

    public void setJuego(GamesWS.Games juego) {
        this.juego = juego;
    }
    
    
    
    
}
