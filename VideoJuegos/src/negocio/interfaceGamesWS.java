/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import GamesWS.Games;
import java.util.List;

/**
 *
 * @author jorge
 */
public class interfaceGamesWS {
    private static GamesWS.GamesWS_Service service = new GamesWS.GamesWS_Service();
    private static GamesWS.GamesWS port = service.getGamesWSPort();
    
    public static boolean agregarJuego(java.lang.String nombre, java.lang.String clasificacion, java.lang.String categoria, java.lang.String urlImagen, java.lang.String urlDescarga, java.lang.String valoracion, java.lang.String review) {
        return port.agregarJuego(nombre, clasificacion, categoria, urlImagen, urlDescarga, valoracion, review);
    }

    public static boolean editarJuego(java.lang.String nombre, java.lang.String clasificacion, java.lang.String categoria, java.lang.String urlImagen, java.lang.String urlDescarga, java.lang.String valoracion, java.lang.String review, int id) {
        return port.editarJuego(nombre, clasificacion, categoria, urlImagen, urlDescarga, valoracion, review, id);
    }

    public static boolean eliminarJuego(int id) {
        return port.eliminarJuego(id);
    }

    public static Games obtenerJuegoId(int id) {
        return port.obtenerJuegoId(id);
    }

    public static List<Games> obtenerTodosJuegos() {
        return port.obtenerTodosJuegos();
    }

    public static List<Games> obtenerJuegoNombre(java.lang.String nombre) {
        return port.obtenerJuegoNombre(nombre);
    }
    
    
}
