
package GamesWS;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the GamesWS package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EditarJuegoResponse_QNAME = new QName("http://webservices/", "editarJuegoResponse");
    private final static QName _AgregarJuego_QNAME = new QName("http://webservices/", "agregarJuego");
    private final static QName _ObtenerJuegoIdResponse_QNAME = new QName("http://webservices/", "obtenerJuegoIdResponse");
    private final static QName _ObtenerTodosJuegosResponse_QNAME = new QName("http://webservices/", "obtenerTodosJuegosResponse");
    private final static QName _ObtenerTodosJuegos_QNAME = new QName("http://webservices/", "obtenerTodosJuegos");
    private final static QName _ObtenerJuegoNombreResponse_QNAME = new QName("http://webservices/", "ObtenerJuegoNombreResponse");
    private final static QName _EliminarJuego_QNAME = new QName("http://webservices/", "eliminarJuego");
    private final static QName _EliminarJuegoResponse_QNAME = new QName("http://webservices/", "eliminarJuegoResponse");
    private final static QName _ObtenerJuegoNombre_QNAME = new QName("http://webservices/", "ObtenerJuegoNombre");
    private final static QName _EditarJuego_QNAME = new QName("http://webservices/", "editarJuego");
    private final static QName _AgregarJuegoResponse_QNAME = new QName("http://webservices/", "agregarJuegoResponse");
    private final static QName _ObtenerJuegoId_QNAME = new QName("http://webservices/", "obtenerJuegoId");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: GamesWS
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ObtenerJuegoId }
     * 
     */
    public ObtenerJuegoId createObtenerJuegoId() {
        return new ObtenerJuegoId();
    }

    /**
     * Create an instance of {@link AgregarJuegoResponse }
     * 
     */
    public AgregarJuegoResponse createAgregarJuegoResponse() {
        return new AgregarJuegoResponse();
    }

    /**
     * Create an instance of {@link ObtenerJuegoNombre }
     * 
     */
    public ObtenerJuegoNombre createObtenerJuegoNombre() {
        return new ObtenerJuegoNombre();
    }

    /**
     * Create an instance of {@link EditarJuego }
     * 
     */
    public EditarJuego createEditarJuego() {
        return new EditarJuego();
    }

    /**
     * Create an instance of {@link EliminarJuego }
     * 
     */
    public EliminarJuego createEliminarJuego() {
        return new EliminarJuego();
    }

    /**
     * Create an instance of {@link EliminarJuegoResponse }
     * 
     */
    public EliminarJuegoResponse createEliminarJuegoResponse() {
        return new EliminarJuegoResponse();
    }

    /**
     * Create an instance of {@link ObtenerJuegoNombreResponse }
     * 
     */
    public ObtenerJuegoNombreResponse createObtenerJuegoNombreResponse() {
        return new ObtenerJuegoNombreResponse();
    }

    /**
     * Create an instance of {@link ObtenerTodosJuegos }
     * 
     */
    public ObtenerTodosJuegos createObtenerTodosJuegos() {
        return new ObtenerTodosJuegos();
    }

    /**
     * Create an instance of {@link ObtenerTodosJuegosResponse }
     * 
     */
    public ObtenerTodosJuegosResponse createObtenerTodosJuegosResponse() {
        return new ObtenerTodosJuegosResponse();
    }

    /**
     * Create an instance of {@link ObtenerJuegoIdResponse }
     * 
     */
    public ObtenerJuegoIdResponse createObtenerJuegoIdResponse() {
        return new ObtenerJuegoIdResponse();
    }

    /**
     * Create an instance of {@link AgregarJuego }
     * 
     */
    public AgregarJuego createAgregarJuego() {
        return new AgregarJuego();
    }

    /**
     * Create an instance of {@link EditarJuegoResponse }
     * 
     */
    public EditarJuegoResponse createEditarJuegoResponse() {
        return new EditarJuegoResponse();
    }

    /**
     * Create an instance of {@link Games }
     * 
     */
    public Games createGames() {
        return new Games();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditarJuegoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "editarJuegoResponse")
    public JAXBElement<EditarJuegoResponse> createEditarJuegoResponse(EditarJuegoResponse value) {
        return new JAXBElement<EditarJuegoResponse>(_EditarJuegoResponse_QNAME, EditarJuegoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarJuego }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "agregarJuego")
    public JAXBElement<AgregarJuego> createAgregarJuego(AgregarJuego value) {
        return new JAXBElement<AgregarJuego>(_AgregarJuego_QNAME, AgregarJuego.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerJuegoIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "obtenerJuegoIdResponse")
    public JAXBElement<ObtenerJuegoIdResponse> createObtenerJuegoIdResponse(ObtenerJuegoIdResponse value) {
        return new JAXBElement<ObtenerJuegoIdResponse>(_ObtenerJuegoIdResponse_QNAME, ObtenerJuegoIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerTodosJuegosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "obtenerTodosJuegosResponse")
    public JAXBElement<ObtenerTodosJuegosResponse> createObtenerTodosJuegosResponse(ObtenerTodosJuegosResponse value) {
        return new JAXBElement<ObtenerTodosJuegosResponse>(_ObtenerTodosJuegosResponse_QNAME, ObtenerTodosJuegosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerTodosJuegos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "obtenerTodosJuegos")
    public JAXBElement<ObtenerTodosJuegos> createObtenerTodosJuegos(ObtenerTodosJuegos value) {
        return new JAXBElement<ObtenerTodosJuegos>(_ObtenerTodosJuegos_QNAME, ObtenerTodosJuegos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerJuegoNombreResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "ObtenerJuegoNombreResponse")
    public JAXBElement<ObtenerJuegoNombreResponse> createObtenerJuegoNombreResponse(ObtenerJuegoNombreResponse value) {
        return new JAXBElement<ObtenerJuegoNombreResponse>(_ObtenerJuegoNombreResponse_QNAME, ObtenerJuegoNombreResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarJuego }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "eliminarJuego")
    public JAXBElement<EliminarJuego> createEliminarJuego(EliminarJuego value) {
        return new JAXBElement<EliminarJuego>(_EliminarJuego_QNAME, EliminarJuego.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EliminarJuegoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "eliminarJuegoResponse")
    public JAXBElement<EliminarJuegoResponse> createEliminarJuegoResponse(EliminarJuegoResponse value) {
        return new JAXBElement<EliminarJuegoResponse>(_EliminarJuegoResponse_QNAME, EliminarJuegoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerJuegoNombre }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "ObtenerJuegoNombre")
    public JAXBElement<ObtenerJuegoNombre> createObtenerJuegoNombre(ObtenerJuegoNombre value) {
        return new JAXBElement<ObtenerJuegoNombre>(_ObtenerJuegoNombre_QNAME, ObtenerJuegoNombre.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditarJuego }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "editarJuego")
    public JAXBElement<EditarJuego> createEditarJuego(EditarJuego value) {
        return new JAXBElement<EditarJuego>(_EditarJuego_QNAME, EditarJuego.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarJuegoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "agregarJuegoResponse")
    public JAXBElement<AgregarJuegoResponse> createAgregarJuegoResponse(AgregarJuegoResponse value) {
        return new JAXBElement<AgregarJuegoResponse>(_AgregarJuegoResponse_QNAME, AgregarJuegoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenerJuegoId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices/", name = "obtenerJuegoId")
    public JAXBElement<ObtenerJuegoId> createObtenerJuegoId(ObtenerJuegoId value) {
        return new JAXBElement<ObtenerJuegoId>(_ObtenerJuegoId_QNAME, ObtenerJuegoId.class, null, value);
    }

}
