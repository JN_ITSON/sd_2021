/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import entidades.Games;
import java.util.List;

/**
 *
 * @author vinko
 */
public interface DAOGames {
    public boolean agregar(Games alumno);
    public boolean editar(int id, Games alumno);
    public boolean eliminar(int id);
    public List<Games> obtenerTodos();
    public List<Games> obtenerPorNombre(String nombre);
    public Games obtenerPorId(int id);
    
}
