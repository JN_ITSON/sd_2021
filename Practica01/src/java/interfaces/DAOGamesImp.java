/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import configuracion.DBHelper;
import entidades.Games;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vinko
 */
public class DAOGamesImp implements DAOGames{

    private DBHelper db = new DBHelper();

    @Override
    public boolean agregar(Games game) {
        boolean resultado = false;

        try {
            if (db.connect()) {
                String sql = "INSERT INTO game (nombre,clasificacion,categoria,url_imagen,url_descarga,valoracion,review) "
                        + " VALUES ("
                        + "'" + game.getNombre() + "',"
                        + "'" + game.getClasificacion() + "',"
                        + "'" + game.getCategoria() + "',"
                        + "'" + game.getUrl_imagen() + "',"
                        + "'" + game.getUrl_descarga() + "',"
                        + "'" + game.getValoracion() + "',"
                        + "'" + game.getReview() + "'"
                        + ")";

                resultado = (boolean) db.execute(sql, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.disconnect();  
        }

        return resultado;
    }

    @Override
    public boolean editar(int id, Games game) {

        boolean resultado = false;

        try {
            if (db.connect()) {
                String sql = "UPDATE game SET "
                        + " nombre = '" + game.getNombre() + "',"
                        + " clasificacion = '" + game.getClasificacion() + "',"
                        + " categoria = '" + game.getCategoria() + "',"
                        + " url_imagen = '" + game.getUrl_imagen() + "',"
                        + " url_descarga = '" + game.getUrl_descarga() + "',"
                        + " valoracion = '" + game.getValoracion() + "',"
                        + " review = '" + game.getReview() + "'"
                        + " WHERE id = " + id;

                resultado = (boolean) db.execute(sql, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.disconnect();
        }

        return resultado;
    }

    @Override
    public boolean eliminar(int id) {

        boolean resultado = false;

        try {
            if (db.connect()) {
                String sql = "DELETE FROM game "
                        + " WHERE id = " + id;

                resultado = (boolean) db.execute(sql, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.disconnect();
        }

        return resultado;
    }

    @Override
    public List<Games> obtenerTodos() {
        List<Games> juegos = new ArrayList();

        try {
            if (db.connect()) {
                String sql = "SELECT * FROM game  ";

                ResultSet rs = (ResultSet) db.execute(sql, false);

                while (rs.next()) {
                    Games juego = new Games();

                    juego.setId(rs.getInt("id"));
                    juego.setNombre(rs.getString("nombre"));
                    juego.setClasificacion(rs.getString("clasificacion"));
                    juego.setCategoria(rs.getString("categoria"));
                    juego.setUrl_imagen(rs.getString("url_imagen"));
                    juego.setUrl_descarga(rs.getString("url_descarga"));
                    juego.setValoracion(rs.getString("valoracion"));
                    juego.setReview(rs.getString("review"));

                    juegos.add(juego);

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.disconnect();
        }

        return juegos;
    }

    @Override
    public List<Games> obtenerPorNombre(String nombre) {
        List<Games> juegos = new ArrayList();

        try {
            if (db.connect()) {
                String sql = "SELECT * FROM game WHERE"
                        + " nombre LIKE '%" + nombre + "%' ";

                ResultSet rs = (ResultSet) db.execute(sql, false);

                while (rs.next()) {
                    Games juego = new Games();

                    juego.setId(rs.getInt("id"));
                    juego.setNombre(rs.getString("nombre"));
                    juego.setClasificacion(rs.getString("clasificacion"));
                    juego.setCategoria(rs.getString("categoria"));
                    juego.setUrl_imagen(rs.getString("url_imagen"));
                    juego.setUrl_descarga(rs.getString("url_descarga"));
                    juego.setValoracion(rs.getString("valoracion"));
                    juego.setReview(rs.getString("review"));

                    juegos.add(juego);

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.disconnect();
        }

        return juegos;
    }

    @Override
    public Games obtenerPorId(int id) {
        Games juego = new Games();
        try {
            if (db.connect()) {
                String sql = "SELECT * FROM game WHERE id = " + id;

                ResultSet rs = (ResultSet) db.execute(sql, false);

                if (rs.next()) {
                    juego.setId(rs.getInt("id"));
                    juego.setNombre(rs.getString("nombre"));
                    juego.setClasificacion(rs.getString("clasificacion"));
                    juego.setCategoria(rs.getString("categoria"));
                    juego.setUrl_imagen(rs.getString("url_imagen"));
                    juego.setUrl_descarga(rs.getString("url_descarga"));
                    juego.setValoracion(rs.getString("valoracion"));
                    juego.setReview(rs.getString("review"));
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.disconnect();
        }

        return juego;
    }
}
