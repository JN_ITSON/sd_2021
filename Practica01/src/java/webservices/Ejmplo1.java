package webservices;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

@WebService(serviceName = "Ejmplo1")
public class Ejmplo1 {

    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hola " + txt + " !";
    }
    
    @WebMethod(operationName = "suma")
    public double suma(
            @WebParam(name = "op1")  double op1,
            @WebParam(name = "op2") double op2
    ){
       
        return op1+op2;
    }
}
