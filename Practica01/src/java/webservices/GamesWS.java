/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webservices;

import entidades.Games;
import interfaces.DAOGamesImp;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author vinko
 */
@WebService(serviceName = "GamesWS")
public class GamesWS {

    private DAOGamesImp dao = new DAOGamesImp();

    @WebMethod(operationName = "agregarJuego")
    public boolean agregarJuego(
            @WebParam(name = "nombre") String nombre,
            @WebParam(name = "clasificacion") String clasificacion,
            @WebParam(name = "categoria") String categoria,
            @WebParam(name = "url_imagen") String url_imagen,
            @WebParam(name = "url_descarga") String url_descarga,
            @WebParam(name = "valoracion") String valoracion,
            @WebParam(name = "review") String review
    ) {

        Games game = new Games();
        game.setNombre(nombre);
        game.setClasificacion(clasificacion);
        game.setCategoria(categoria);
        game.setUrl_imagen(url_imagen);
        game.setUrl_descarga(url_descarga);
        game.setValoracion(valoracion);
        game.setReview(review);

        return dao.agregar(game);
    }

    @WebMethod(operationName = "obtenerTodosJuegos")
    public List<Games> obtenerTodosJuegos() {
        return dao.obtenerTodos();
    }

    @WebMethod(operationName = "editarJuego")
    public boolean editarJuego(
            @WebParam(name = "nombre") String nombre,
            @WebParam(name = "clasificacion") String clasificacion,
            @WebParam(name = "categoria") String categoria,
            @WebParam(name = "url_imagen") String url_imagen,
            @WebParam(name = "url_descarga") String url_descarga,
            @WebParam(name = "valoracion") String valoracion,
            @WebParam(name = "review") String review,
            @WebParam(name = "id") int id
    ) {

        Games juego = new Games();
        juego.setNombre(nombre);
        juego.setClasificacion(clasificacion);
        juego.setCategoria(categoria);
        juego.setUrl_imagen(url_imagen);
        juego.setUrl_descarga(url_descarga);
        juego.setValoracion(valoracion);
        juego.setReview(review);

        return dao.editar(id, juego);
    }

    @WebMethod(operationName = "eliminarJuego")
    public boolean eliminarJuego(
            @WebParam(name = "id") int id
    ) {
        return dao.eliminar(id);
    }

    @WebMethod(operationName = "ObtenerJuegoNombre")
    public List<Games> ObtenerJuegoNombre(
            @WebParam(name = "nombre") String nombre
    ) {

        Games juego = new Games();
        juego.setNombre(nombre);

        return dao.obtenerPorNombre(nombre);
    }

    @WebMethod(operationName = "obtenerJuegoId")
    public Games obtenerJuegoId(
            @WebParam(name = "id") int id
    ) {

        return dao.obtenerPorId(id);
    }

}
