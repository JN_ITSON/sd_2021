/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente_ws_01;

/**
 *
 * @author jorge
 */
public class Cliente_WS_01 {
    private static Ejmplo1WS.Ejmplo1_Service service = new Ejmplo1WS.Ejmplo1_Service();
    private static  Ejmplo1WS.Ejmplo1 port = service.getEjmplo1Port();
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        String webserviceReturn  = port.hello("World");
        System.out.println("El web service se ha consumido.");
        System.out.println(webserviceReturn);
        
        
        double op1 = 5.5;
        double op2 = 13.45;
        
        double suma = port.suma(op1, op2);
        
        System.out.println("La suma  "+op1+"+"+op2+" = "+suma);
        
        
    }

}
