
package entidades;

import java.util.ArrayList;
import java.util.List;


public class Usuario {
    private int id = 0;
    private String nombre = new String();
    private String email = new String();
    private String contrasena = new String();
    private String nickname = new String();
    private String perfil = new String();
    private List<Juego> favoritos = new ArrayList();

    public Usuario() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public List<Juego> getFavoritos() {
        return favoritos;
    }

    public void setFavoritos(List<Juego> favoritos) {
        this.favoritos = favoritos;
    }
    
    

}
