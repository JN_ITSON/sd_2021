package entidades;

import java.util.ArrayList;
import java.util.List;

public class Juego {
    private int id  = 0;
    private String nombre = new String();
    private String version = new String();
    private double valoracion = 0.0;
    private String lugar_creacion = new String();
    private String fecha_creacion = new String();
    private double precio  = 0.0;
    private String enlace_descarga  = new String();
    private String enlace_poster  = new String();
    private String enlace_cover  = new String();
    private Categoria categoria = new Categoria();
    private String descripcion  = new String();
    private String clasificacion = new String();
    private List<Categoria> categoriasSecundarias = new ArrayList();

    public Juego() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public double getValoracion() {
        return valoracion;
    }

    public void setValoracion(double valoracion) {
        this.valoracion = valoracion;
    }

    public String getLugar_creacion() {
        return lugar_creacion;
    }

    public void setLugar_creacion(String lugar_creacion) {
        this.lugar_creacion = lugar_creacion;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getEnlace_descarga() {
        return enlace_descarga;
    }

    public void setEnlace_descarga(String enlace_descarga) {
        this.enlace_descarga = enlace_descarga;
    }

    public String getEnlace_poster() {
        return enlace_poster;
    }

    public void setEnlace_poster(String enlace_poster) {
        this.enlace_poster = enlace_poster;
    }

    public String getEnlace_cover() {
        return enlace_cover;
    }

    public void setEnlace_cover(String enlace_cover) {
        this.enlace_cover = enlace_cover;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public List<Categoria> getCategoriasSecundarias() {
        return categoriasSecundarias;
    }

    public void setCategoriasSecundarias(List<Categoria> categoriasSecundarias) {
        this.categoriasSecundarias = categoriasSecundarias;
    }
    
    
    
}
