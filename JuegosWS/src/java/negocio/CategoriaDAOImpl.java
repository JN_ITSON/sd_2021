
package negocio;

import configuracion.DBHelper;
import entidades.Categoria;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CategoriaDAOImpl implements CRUD<Categoria> {
    
    DBHelper db = new DBHelper();

    @Override
    public boolean agregar(Categoria cat) {
        boolean result = false;
       
        try {
            
            if(db.connect()){
                //Logica de insercion/mod/eli
                StringBuilder sql = new StringBuilder();
                //Generar mi consulta
                sql.append("INSERT INTO categoria ")
                   .append(" (nombre,descripcion) VALUES ( ")     
                   .append("'").append(cat.getNombre()).append("',")
                   .append("'").append(cat.getDescripcion()).append("' ")     
                   .append(")")
                        ;
                result = (boolean) db.execute(sql.toString(), true);
                
            }else{
                System.out.println("ERROR: "+db.getError());   
            }            
        } catch (Exception e) {
             System.out.println("ERROR: "+e.getMessage());  
        }finally{
            db.disconnect();
        }
        
        return result;
    }

    @Override
    public boolean editar(Categoria cat) {
        boolean result = false;
               
        try {
            
            if(db.connect()){
                //Logica de insercion/mod/eli
                StringBuilder sql = new StringBuilder();
                //Generar mi consulta
                sql.append("UPDATE categoria ")
                   .append(" SET ")     
                   .append("nombre = '").append(cat.getNombre()).append("',")
                   .append("descripcion = '").append(cat.getDescripcion()).append("' ")     
                   .append(" WHERE id = ").append(cat.getId())
                        ;
                result = (boolean) db.execute(sql.toString(), true);
                
            }else{
                System.out.println("ERROR: "+db.getError());   
            }            
        } catch (Exception e) {
             System.out.println("ERROR: "+e.getMessage());  
        }finally{
            db.disconnect();
        }
        
        return result;
    }

    @Override
    public boolean eliminar(int id) {
        boolean result = false;
        
        try {
            
            if(db.connect()){
                //Logica de insercion/mod/eli
                StringBuilder sql = new StringBuilder();
                //Generar mi consulta
                sql.append("DELETE FROM categoria WHERE id = ").append(id);
                result = (boolean) db.execute(sql.toString(), true);
                
            }else{
                System.out.println("ERROR: "+db.getError());   
            }            
        } catch (Exception e) {
             System.out.println("ERROR: "+e.getMessage());  
        }finally{
            db.disconnect();
        }
        
        return result;
    }

    @Override
    public List<Categoria> buscarTodos() {
       List<Categoria> categorias = new ArrayList();
       
        try {
            
            if(db.connect()){
                //Logica de insercion/mod/eli
                StringBuilder sql = new StringBuilder();
                //Generar consulta
                sql.append("SELECT * FROM categoria");
                
                ResultSet rs = (ResultSet) db.execute(sql.toString(), false);
                
                while(rs.next()){
                    Categoria cat = new Categoria();
                    
                    cat.setId(rs.getInt("id"));
                    cat.setNombre(rs.getString("nombre"));
                    cat.setDescripcion(rs.getString("descripcion"));
                    
                    categorias.add(cat);
                    
                }
                
            }else{
                System.out.println("ERROR: "+db.getError());   
            }            
        } catch (Exception e) {
             System.out.println("ERROR: "+e.getMessage());  
        }finally{
            db.disconnect();
        }
       
       return categorias;
    }

    @Override
    public List<Categoria> buscarFiltro(String filtro, String valor) {
        List<Categoria> categorias = new ArrayList();
       
        try {
            
            if(db.connect()){
                //Logica de insercion/mod/eli
                StringBuilder sql = new StringBuilder();
                //Generar consulta
                sql.append("SELECT * FROM categoria ")
                   .append(" WHERE ").append(filtro)
                        .append(" LIKE '%").append(valor).append("%';")     
                        ;
                
                ResultSet rs = (ResultSet) db.execute(sql.toString(), false);
                
                while(rs.next()){
                    Categoria cat = new Categoria();
                    
                    cat.setId(rs.getInt("id"));
                    cat.setNombre(rs.getString("nombre"));
                    cat.setDescripcion(rs.getString("descripcion"));
                    
                    categorias.add(cat);
                    
                }
                
            }else{
                System.out.println("ERROR: "+db.getError());   
            }            
        } catch (Exception e) {
             System.out.println("ERROR: "+e.getMessage());  
        }finally{
            db.disconnect();
        }
       
       return categorias;
    }

    @Override
    public Categoria buscarId(int id) {
        
        Categoria cat = new Categoria();
        
        try {
            
            if(db.connect()){
                //Logica de insercion/mod/eli
                StringBuilder sql = new StringBuilder();
                //Generar consulta
                sql.append("SELECT * FROM categoria ")
                   .append(" WHERE id = ").append(id).append(";")     
                        ;
                
                ResultSet rs = (ResultSet) db.execute(sql.toString(), false);
                
                if(rs.next()){
                    
                    cat.setId(rs.getInt("id"));
                    cat.setNombre(rs.getString("nombre"));
                    cat.setDescripcion(rs.getString("descripcion"));
                                        
                }
                
            }else{
                System.out.println("ERROR: "+db.getError());   
            }            
        } catch (Exception e) {
             System.out.println("ERROR: "+e.getMessage());  
        }finally{
            db.disconnect();
        }
       
       return cat;
    }
    
}
