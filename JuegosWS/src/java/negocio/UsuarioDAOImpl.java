
package negocio;

import configuracion.DBHelper;
import entidades.Categoria;
import entidades.Juego;
import entidades.Usuario;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class UsuarioDAOImpl implements CRUD<Usuario>{
    
    DBHelper db = new DBHelper();
    
    @Override
    public boolean agregar(Usuario user) {
        boolean result = false;
       
        try {
            
            if(db.connect()){
                //Logica de insercion/mod/eli
                StringBuilder sql = new StringBuilder();
                //Generar mi consulta
                sql.append("INSERT INTO usuario ")
                   .append(" (nombre,email,contrasena,nickname,perfil) VALUES ( ")     
                   .append("'").append(user.getNombre()).append("',")
                   .append("'").append(user.getEmail()).append("', ") 
                   .append(" MD5('").append(user.getContrasena()).append("'), ")
                   .append("'").append(user.getNickname()).append("', ") 
                   .append("'").append(user.getPerfil()).append("' ") 
                   .append(")")
                        ;
                result = (boolean) db.execute(sql.toString(), true);
                
            }else{
                System.out.println("ERROR: "+db.getError());   
            }            
        } catch (Exception e) {
             System.out.println("ERROR: "+e.getMessage());  
        }finally{
            db.disconnect();
        }
        
        return result;
    }

    @Override
    public boolean editar(Usuario user) {
        boolean result = false;
       
        try {
            
            if(db.connect()){
                //Logica de insercion/mod/eli
                StringBuilder sql = new StringBuilder();
                //Generar mi consulta
                sql.append("UPDATE usuario ")
                   .append(" SET ")     
                   .append(" nombre = '").append(user.getNombre()).append("',")
                   .append(" email = '").append(user.getEmail()).append("', ") 
                   //.append(" contrasena = MD5('").append(user.getContrasena()).append("'), ")
                   .append(" nickname = '").append(user.getNickname()).append("', ") 
                   .append(" perfil = '").append(user.getPerfil()).append("' ") 
                   .append(" WHERE id = ").append(user.getId())
                        ;
                result = (boolean) db.execute(sql.toString(), true);
                
            }else{
                System.out.println("ERROR: "+db.getError());   
            }            
        } catch (Exception e) {
             System.out.println("ERROR: "+e.getMessage());  
        }finally{
            db.disconnect();
        }
        
        return result;
    }

    @Override
    public boolean eliminar(int id) {
        boolean result = false;
       
        try {
            
            if(db.connect()){
                //Logica de insercion/mod/eli
                StringBuilder sql = new StringBuilder();
                //Generar mi consulta
                sql.append("DELETE FROM usuario ")
                  .append(" WHERE id = ").append(id)
                        ;
                result = (boolean) db.execute(sql.toString(), true);
                
            }else{
                System.out.println("ERROR: "+db.getError());   
            }            
        } catch (Exception e) {
             System.out.println("ERROR: "+e.getMessage());  
        }finally{
            db.disconnect();
        }
        
        return result;
    }

    @Override
    public List<Usuario> buscarTodos() {
        List<Usuario> usuarios = new ArrayList();
        
        try {
            
            if(db.connect()){
                //Logica de insercion/mod/eli
                StringBuilder sql = new StringBuilder();
                //Generar mi consulta
                sql.append("SELECT * FROM usuario ");
                
                ResultSet rs = (ResultSet) db.execute(sql.toString(),false);
                
                while(rs.next()){
                    Usuario user = new Usuario();
                    
                    user.setId(rs.getInt("id"));
                    user.setNombre(rs.getString("nombre"));
                    user.setEmail(rs.getString("email"));
                    user.setNickname(rs.getString("nickname"));
                    user.setPerfil(rs.getString("perfil"));
                    
                    usuarios.add(user);
                    
                }
                
            }else{
                System.out.println("ERROR: "+db.getError());   
            }            
        } catch (Exception e) {
             System.out.println("ERROR: "+e.getMessage());  
        }finally{
            db.disconnect();
        }
        
        
        return usuarios;
    }

    @Override
    public List<Usuario> buscarFiltro(String filtro, String valor) {
         List<Usuario> usuarios = new ArrayList();
        
        try {
            
            if(db.connect()){
                //Logica de insercion/mod/eli
                StringBuilder sql = new StringBuilder();
                //Generar mi consulta
                sql.append("SELECT * FROM usuario ")
                   .append(" WHERE ").append(filtro)
                        .append(" LIKE '%").append(valor).append("%';")     
                        ;
                
                ResultSet rs = (ResultSet) db.execute(sql.toString(),false);
                
                while(rs.next()){
                    Usuario user = new Usuario();
                    
                    user.setId(rs.getInt("id"));
                    user.setNombre(rs.getString("nombre"));
                    user.setEmail(rs.getString("email"));
                    user.setNickname(rs.getString("nickname"));
                    user.setPerfil(rs.getString("perfil"));
                    
                    usuarios.add(user);
                    
                }
                
            }else{
                System.out.println("ERROR: "+db.getError());   
            }            
        } catch (Exception e) {
             System.out.println("ERROR: "+e.getMessage());  
        }finally{
            db.disconnect();
        }
        
        
        return usuarios;
    }

    @Override
    public Usuario buscarId(int id) {
       
        Usuario user = new Usuario();
        
        try {
            
            if(db.connect()){
                //Logica de insercion/mod/eli
                StringBuilder sql = new StringBuilder();
                //Generar mi consulta
                sql.append("SELECT * FROM usuario ")
                   .append(" WHERE id = ").append(id)    
                        ;
                
                ResultSet rs = (ResultSet) db.execute(sql.toString(),false);
                
                if(rs.next()){
                    
                    user.setId(rs.getInt("id"));
                    user.setNombre(rs.getString("nombre"));
                    user.setEmail(rs.getString("email"));
                    user.setNickname(rs.getString("nickname"));
                    user.setPerfil(rs.getString("perfil"));
                                        
                }
                
            }else{
                System.out.println("ERROR: "+db.getError());   
            }            
        } catch (Exception e) {
             System.out.println("ERROR: "+e.getMessage());  
        }finally{
            db.disconnect();
        }
        
        
        return user;
    }
    
    
    public Usuario login(String email, String contrasena){
        
        Usuario user = new Usuario();
        
        try {
            
            if(db.connect()){
                //Logica de insercion/mod/eli
                StringBuilder sql = new StringBuilder();
                //Generar mi consulta
                sql.append("SELECT * FROM usuario ")
                   .append(" WHERE email LIKE '").append(email).append("' ") 
                        .append(" AND contrasena LIKE MD5('").append(contrasena).append("')")
                        ;
                
                ResultSet rs = (ResultSet) db.execute(sql.toString(),false);
                
                if(rs.next()){
                    
                    user.setId(rs.getInt("id"));
                    user.setNombre(rs.getString("nombre"));
                    user.setEmail(rs.getString("email"));
                    user.setNickname(rs.getString("nickname"));
                    user.setPerfil(rs.getString("perfil"));
                                        
                }
                
            }else{
                System.out.println("ERROR: "+db.getError());   
            }            
        } catch (Exception e) {
             System.out.println("ERROR: "+e.getMessage());  
        }finally{
            db.disconnect();
        }
        
        
        return user;
    }
    
    public List<Juego> obtenerFavoritos(int idUser){
         List<Juego> favoritos = new ArrayList();
        
        try {
            
            if(db.connect()){
                //Logica de insercion/mod/eli
                StringBuilder sql = new StringBuilder();
                //Generar mi consulta
                sql.append("SELECT juego.*,categoria.nombre AS 'nombreCategoria' FROM favorito ")
                   .append(" LEFT JOIN juego ON juego.id = favorito.juego ")
                   .append(" LEFT JOIN categoria ON categoria.id = juego.categoria ")
                   .append(" WHERE favorito.usuario = ").append(idUser).append(";")     
                        ;
                
                ResultSet rs = (ResultSet) db.execute(sql.toString(),false);
                
                while(rs.next()){
                    Juego juego = new Juego();
                    Categoria cat = new Categoria();
                    
                    
                    juego.setId(rs.getInt("id"));
                    juego.setNombre(rs.getString("nombre"));
                    cat.setNombre(rs.getString("nombreCategoria"));
                    cat.setId(rs.getInt("categoria"));
                    juego.setCategoria(cat);
                    
                    
                    favoritos.add(juego);
                    
                }
                
            }else{
                System.out.println("ERROR: "+db.getError());   
            }            
        } catch (Exception e) {
             System.out.println("ERROR: "+e.getMessage());  
        }finally{
            db.disconnect();
        }
        
        
        return favoritos;
    }
    
    
    public boolean agregarFavorito(int idUser,int idJuego){
        return true;
    }
    
    public boolean eliminarFavorito(int idFav){
        return true;
    }
    
}
