
package webservices;

import entidades.Categoria;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import negocio.CategoriaDAOImpl;

@WebService(serviceName = "CategoriaWS")
public class CategoriaWS {

    CategoriaDAOImpl dao = new CategoriaDAOImpl();
    
    @WebMethod(operationName = "agregarCategoria")
    public boolean agregarCategoria(
            @WebParam(name = "nombre") String nombre,
            @WebParam(name = "descripcion") String descripcion
    ) {
        
        Categoria cat = new Categoria();
        cat.setNombre(nombre);
        cat.setDescripcion(descripcion);
        
        return dao.agregar(cat);
    }
    
    @WebMethod(operationName = "editarCategoria")
    public boolean editarCategoria(
            @WebParam(name = "nombre") String nombre,
            @WebParam(name = "descripcion") String descripcion,
            @WebParam(name = "id") int id
    ) {
        
        Categoria cat = new Categoria();
        cat.setId(id);
        cat.setNombre(nombre);
        cat.setDescripcion(descripcion);
        
        return dao.editar(cat);
    }
    
    
    @WebMethod(operationName = "eliminarCategoria")
    public boolean eliminarCategoria(
            @WebParam(name = "id") int id
    ) {
                
        return dao.eliminar(id);
    }
    
    @WebMethod(operationName = "buscarTodosCategoria")
    public List<Categoria> buscarTodosCategoria(){
        return dao.buscarTodos();
    }
    
    @WebMethod(operationName = "buscarFiltroCategoria")
    public List<Categoria> buscarFiltroCategoria(
            @WebParam(name = "filtro") String filtro,
            @WebParam(name = "valor") String valor
    ){
        return dao.buscarFiltro(filtro, valor);
    }
    
    @WebMethod(operationName = "buscarIdCategoria")
    public Categoria buscarIdCategoria(
            @WebParam(name = "id") int id
    ){
        return dao.buscarId(id);
    }
    
    
}
