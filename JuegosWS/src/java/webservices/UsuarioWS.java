package webservices;

import entidades.Juego;
import entidades.Usuario;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import negocio.UsuarioDAOImpl;

/**
 *
 * @author jorge
 */
@WebService(serviceName = "UsuarioWS")
public class UsuarioWS {

   UsuarioDAOImpl dao = new UsuarioDAOImpl();
    
    @WebMethod(operationName = "agregarUsuario")
    public boolean agregarUsuario(
            @WebParam(name = "nombre") String nombre,
            @WebParam(name = "descripcion") String descripcion
    ) {
        
        Usuario cat = new Usuario();
//        cat.setNombre(nombre);
//        cat.setDescripcion(descripcion);
        
        return dao.agregar(cat);
    }
    
    @WebMethod(operationName = "editarUsuario")
    public boolean editarUsuario(
            @WebParam(name = "nombre") String nombre,
            @WebParam(name = "descripcion") String descripcion,
            @WebParam(name = "id") int id
    ) {
        
        Usuario cat = new Usuario();
        cat.setId(id);
        cat.setNombre(nombre);
//        cat.setDescripcion(descripcion);
        
        return dao.editar(cat);
    }
    
    
    @WebMethod(operationName = "eliminarUsuario")
    public boolean eliminarUsuario(
            @WebParam(name = "id") int id
    ) {
                
        return dao.eliminar(id);
    }
    
    @WebMethod(operationName = "buscarTodosUsuario")
    public List<Usuario> buscarTodosUsuario(){
        return dao.buscarTodos();
    }
    
    @WebMethod(operationName = "buscarFiltroUsuario")
    public List<Usuario> buscarFiltroUsuario(
            @WebParam(name = "filtro") String filtro,
            @WebParam(name = "valor") String valor
    ){
        return dao.buscarFiltro(filtro, valor);
    }
    
    @WebMethod(operationName = "buscarIdUsuario")
    public Usuario buscarIdUsuario(
            @WebParam(name = "id") int id
    ){
        return dao.buscarId(id);
    }
    
    
    @WebMethod(operationName = "login")
    public Usuario login(
            @WebParam(name = "email") String email,
            @WebParam(name = "contrasena") String contrasena
    ){
        return dao.login(email, contrasena);
    }
    
    @WebMethod(operationName = "obtenerFavoritosUsuario")
    public List<Juego> obtenerFavoritosUsuario(
            @WebParam(name = "idUser") int idUser
    ){
        return dao.obtenerFavoritos(idUser);
    }
    
}
